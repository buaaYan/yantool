package util

import "path/filepath"

// git pull
func GitPull(r, c, detail bool) error {
	if c {
		err := GitClean(detail)
		if err != nil {
			return err
		}
	}
	if r {
		err := GitClean(detail)
		if err != nil {
			return err
		}
		return RunShell(detail, "git", "pull", "-r")
	} else {
		return RunShell(detail, "git", "pull")
	}
}

func GitClone(repositoryUrl string) error {
	return RunCmd("git", "clone", repositoryUrl)
}

func GitRebase(branch string) error {
	return RunCmd("git", "rebase", branch)
}

func GitClean(detail bool) error {
	return RunShell(detail,"git", "checkout", ".")
}

func GitCheckoutBranch(branch string, c, detail bool) error {
	if c {
		if err := GitClean(detail); err != nil {
			return err
		}
	}
	return RunShell(detail, "git", "checkout", branch)
}

func GitIsRepository() (bool, error) {
	dirPath, err := filepath.Abs(".")
	if err != nil {
		return false, err
	}
	gitHiddenPath := filepath.Join(dirPath, ".git")
	return IsExists(gitHiddenPath, "dir"), nil
}
