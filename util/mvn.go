package util

import (
	"path/filepath"
)

func MvnInstall(detail bool) error {
	return RunShell(detail, "mvn", "package")
}

func MvnPackage(skipTest, detail bool) error {
	if skipTest {
		return RunShell(detail, "mvn", "package", "-DskipTests")
	}
	return RunShell(detail, "mvn", "package")
}

func MvnClean(detail bool) error {
	return RunShell(detail, "mvn", "clean")
}

func MvnCleanPackageInstall(skipTest, detail bool) error {
	if skipTest {
		err := RunShell(detail, "mvn", "clean", "package", "-DskipTests", "install")
		return err
	}
	return RunShell(detail, "mvn", "clean", "package", "-DskipTests", "install")
}

func IsMvnProject() (bool, error) {
	dirPath, err := filepath.Abs(".")
	if err != nil {
		return false, err
	}
	gitHiddenPath := filepath.Join(dirPath, "pom.xml")
	return IsExists(gitHiddenPath, "file"), nil
}
