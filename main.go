package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"hhchat.cn/yantool/condef"
)

func main() {
	app := cli.NewApp()
	app.Name = "yantool"
	app.Usage = "帮助快速部署和重启北航同研组件"
	app.Version = condef.VERSION
	app.Author = condef.AUTHOR
	app.Writer = os.Stdout

	// def command
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "home",
			Value:  "/home/ubuntu",
			Usage:  "用户文件夹",
			EnvVar: "HOME",
		},
	}
	// def action
	app.Commands = []cli.Command{
		deployCmd,
		logCmd,
	}

	app.Action = func(c *cli.Context) error {
		homePath := c.String("home")
		log.Printf("欢迎使用同研工具箱: %s", homePath)
		return nil
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}
